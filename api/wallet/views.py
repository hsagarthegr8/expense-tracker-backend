from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from expense_manager.models import Wallet
from .serializers import WalletSerializer


class WalletListView(ListAPIView):
    serializer_class = WalletSerializer

    def get_queryset(self):
        user = self.request.user
        return Wallet.objects.filter(user=user)


class WalletCreateView(CreateAPIView):
    serializer_class = WalletSerializer


class WalletUpdateView(UpdateAPIView):
    serializer_class = WalletSerializer

    def get_queryset(self):
        user = self.request.user
        return Wallet.objects.filter(user=user)


class WalletDeleteView(DestroyAPIView):
    serializer_class = WalletSerializer

    def get_queryset(self):
        user = self.request.user
        return Wallet.objects.filter(user=user)

@api_view(['PATCH'])
def transfer_balance_between_wallets(request):
    source = request.data.get('from')
    source = Wallet.objects.get(id = source)
    destination = request.data.get('to')
    destination = Wallet.objects.get(id = destination)
    balance = request.data.get('balance')

    if source.balance < balance:
        return Response('Bad Request', status=status.HTTP_400_BAD_REQUEST)
    
    source.transfer_money(destination, balance, request.user)

    return Response('OK')